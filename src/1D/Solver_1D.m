%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This program solves the 1D bubble equations of motion and calculates the
%Euclidean action
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc, clear all, close all

%Number of time steps
psteps = 250000; %1000000 for nice graph

%Preallocating
phi=zeros(1,psteps);
d_phi=zeros(1,psteps);
d2_phi=zeros(1,psteps); 
dV = zeros(1,psteps);
p_vector = zeros(1,psteps);

%Constants
d = 3;
dp = 1e-4;
a = 5/8; %0.51; Value for the stiff ode
E = -20; %-1; Value for the stiff ode
S_E = 0; %  Initialising Euclidean action
A_2 = 4*pi;

%Upper and lower bound starting positions for the particle
ub = 1-dp;
lb = 0;

%Solving of the ode
for iter = 2:2
    for n=1:(10^iter) %100 for nice graph

        phi(1) = (ub + lb)/2;
        d_phi(1) = 0;
        d2_phi(1) = 0;
        dV(1) = (4*a-3)*E*phi(1)+3*E*phi(1)^2-4*a*E*phi(1)^3;
        p_vector(1) = 0;

        for p = 1:psteps-1

            p_vector(p+1) = p_vector(p) + dp;
            %fprintf('P_vector %d = %d\n',p+1, p_vector(p+1))
            d2_phi(p+1) = -(d-1)*d_phi(p)/p_vector(p+1)+dV(p);
            %fprintf('d2_phi %d = %d\n',p+1, d2_phi(p+1))
            d_phi(p+1) = d_phi(p)+ d2_phi(p+1)*dp;
            %fprintf('d_phi %d = %d\n',p+1, d_phi(p+1))
            phi(p+1) = phi(p) + d_phi(p)*dp + (d2_phi(p+1)*dp^2)/2;
            %fpfrintf('phi %d = %d\n',p+1, phi(p+1))
            dV(p+1) = (4*a-3)*E*phi(p)+3*E*phi(p)^2-4*a*E*phi(p)^3;
            %fprintf('dV %d = %d\n\n',p+1, dV(p+1))

        end

        if phi(psteps) < 3
            lb = (ub + lb)/2;
        else
            ub = (ub + lb)/2;
        end


    end
    
    figure(iter)
    plot(p_vector,phi,'LineWidth',2)
    hold on
    title('\phi vs \rho')
    xlabel('\rho')
    ylabel('\phi')
    xlim([0 p_vector(psteps-1)])
    ylim([0 1])
    grid on
    hold off
end

%Define potential
V_phi = ((4*a-3)/2)*E*(phi.^2)+E*phi.^3 - a*E*phi.^4; 
V_fv = 0; %false vaccuum kept at zero


%Calculation of the Euclidean action


%Finder ensure integration done up until the metastable position
i = find(phi == min(phi));

integral_kinetic = A_2.*(p_vector.^(d-1)).*(0.5*d_phi.*d_phi);
integral_potential = A_2.*(p_vector.^(d-1)).*(V_phi + V_fv);
for n = i+1:length(integral_kinetic)
    integral_kinetic(n) = 0; %Zeroing out divergent elements
    integral_potential(n) = 0;
end

S_E_int_ke = trapz(p_vector, integral_kinetic);
S_E_int_pe = trapz(p_vector, integral_potential);
se = S_E_int_ke + S_E_int_pe;

fprintf('action = %.8f\n', se)
fprintf('initial phi = %.8f\n', phi(1))




