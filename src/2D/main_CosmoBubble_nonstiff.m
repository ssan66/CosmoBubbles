%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Program:      2D Bubble profile
%
% Author:       Suresh Sangarapillai, Physics Department, Monash University
%
% Created:      August 31, 2017
%
% Purpose:      Calculates:
%
%               Initial position of the 2D bubble equation utlising a path
%               deformation algorithm.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc, clear all, close all

%Preallocation 
del = 0.4;
dim = 2;
input_points = 6;
psteps = 1e3;
pert_num = 50;
x = zeros(1,psteps);
dx = zeros(1,psteps);
d2x = zeros(1,psteps); 
dV = zeros(dim,psteps);
dVx = zeros(1,psteps);
phi_x_calc= zeros(2,psteps);
trueVacMat = ones(1,psteps);
phi_input = zeros(dim,input_points);
falseVarray = zeros(1,psteps);
phi_p = zeros(2,psteps);
tan_p = zeros(2,psteps);
norm_p = zeros(2,psteps);
dphidx_rho = zeros(2,psteps);
phi_t_f = zeros(2,1);
phi_pert = zeros(2,pert_num);
prompt = 'Enter perturbations number (show < 1)';
max_old_NF = zeros(2,1);
max_new_NF = zeros(2,1);
x_min = 1.04637; % Constant used to rescale the potential
y_min = 1.66349; % Constant used to rescale the potential
%End Preallocation

%Constants
d = 3;
dp = 1e-2; %Time step
precision = 1e-4;
i = 1;
ub = 1-dp; %Initial upper bound limit for phi
lb = 0; %Initial lower bound limit for phi
p_vector = dp*[0:psteps-1]; %p matrix
pfactor = 0; %Perturbation factor;
K = 0.000005; %perturbation step 0.000005;
show = 0;
const_iter = 10; %Number of iterations for non recalculated deformations
deformations = 0; %Counter for number of non recalculated deformations
isFirstPert = 1; %Constant to indicate first perturbation (for dyn_perturb fn)

%Potenitial constants
alpha = 1.8;
beta = 0.2; 
gamma_i = 0;
gamma_j = 0;
counter = 0;
iterations = 0;
save = 0;

%False Vacuum (the equation should end here)
phi_input(1,input_points) = 0;
phi_input(2,input_points) = 0;

%Guess points
trueVac = 1;
phi_input(1,2) = 0.8;
phi_input(2,2) = 0.8;%0.8; phi_input(1,2);
fprintf('x = %g, y = %g\n',phi_input(1,2),phi_input(2,2))
phi_input(1,3) = 0.6;
phi_input(2,3) = 0.6; % 0.6;  phi_input(1,3);
fprintf('x = %g, y = %g\n',phi_input(1,3),phi_input(2,3))
phi_input(1,4) = 0.4;
phi_input(2,4) = 0.4; %0.4; phi_input(1,4);
fprintf('x = %g, y = %g\n',phi_input(1,4),phi_input(2,4))
phi_input(1,5) = 0.2;
phi_input(2,5) = 0.2;% 0.04; %0.04; phi_input(1,5);
fprintf('x = %g, y = %g\n',phi_input(1,5),phi_input(2,5))
%End guess points

%Perturbed true vacuum
phi_input(1,1) = trueVac; 
phi_input(2,1) = trueVac;

%Initialising Previous perturbation points
phi_pert_prev = phi_input;

% Use this if you want to input a guess spline
% phi_input = transpose(xlsread('Guess_points_iter_4000.xlsx'));

%Invoking guess spline
[x_array,phi_spline] = phi_x(phi_input, precision, 0, trueVac); %function cannot be used backwards

%Finding length of x_array
len_x_mat = length(x_array);

%Finding tangents of array
tan_array = et(phi_spline,x_array);
tan_len = length(tan_array);
trueVacMat = trueVacMat*x_array(end);

%Finding normals of array
%deltaV = (4*a-3)*E*phi_spline+3*E*phi_spline.^2-4*a*E*phi_spline.^3; %((4*a-3)/2)*E*phi_spline.^2+E*phi_spline.^3-a*E*phi_spline.^4;
deltaV = zeros(2,length(phi_spline(1,:)));
 %addition to potential HERE: gamma_i*(0.25*x.^4 - x.^3/3.) REF cosmo
 %transitions fullTunneling.py

deltaV(1,:) = 2*x_min*phi_spline(1,:).*(alpha*(x_min*phi_spline(1,:)-1).^2+beta*(y_min*phi_spline(2,:)-1).^2-del)+((x_min*phi_spline(1,:)).^2+(y_min*phi_spline(2,:)).^2).*(2*alpha.*(x_min*phi_spline(1,:)-1)) + gamma_i*(0.25*4.*x_min*phi_spline(1,:).^3 - 3*(x_min*phi_spline(1,:).^2)/3);
deltaV(2,:) = 2*y_min*phi_spline(2,:).*(alpha*(x_min*phi_spline(1,:)-1).^2+beta*(y_min*phi_spline(2,:)-1).^2-del)+((x_min*phi_spline(1,:)).^2+(y_min*phi_spline(2,:)).^2).*(2*beta.*(y_min*phi_spline(2,:)-1)) + gamma_j*(0.25*4.*y_min*phi_spline(2,:).^3 - 3*(y_min*phi_spline(2,:).^2)/3);

norm_array = en(tan_array,x_array,deltaV);


%Plotting spline with tangent vectors
plot(phi_spline(1,:),phi_spline(2,:),'k','LineWidth',5)
hold on 
scatter(phi_input(1,:),phi_input(2,:),100,'r')
    
len = length(phi_spline);
pnt = floor(len/5);
    
% qp = [1, pnt, pnt*2, pnt*3, pnt*4, pnt*5];
qp = floor(linspace(1,len-5, 100));

%Tangent and Normal Plot    
figure(1)
quiver(phi_spline(1,qp),phi_spline(2,qp),tan_array(1,qp),tan_array(2,qp))
quiver(phi_spline(1,qp),phi_spline(2,qp),norm_array(1,qp),norm_array(2,qp))
xlabel('\phi_x')
ylabel('\phi_y')
title('Interpolated Spline with tangetial and normal vectors')
grid on
hold off

%Perturbation factor
phi_t_f(1,1) = phi_input(1,1) - phi_input(1,input_points);
phi_t_f(2,1) = phi_input(2,1) - phi_input(2,input_points);
phitf = sqrt(sum(phi_t_f.^2));


%Lower and upper bound of inital x
ubx = x_array(end);
lbx = 0;

%Preallocating normal force array
N_x = zeros(1,len_x_mat);
deltaV_norm = zeros(1,len_x_mat);
dphidx = zeros(2,len_x_mat);
d2phidx2 = zeros(2,len_x_mat);
dxdp_calc = zeros(1,len_x_mat); %dxdp function

for perts = 1:200000
    iterations = perts
    counter = counter + 1;
    %Equation 4 calculation
    breakout = 0;
    n = 0;
    while breakout == 0 

        %Updates starting position
        x(1) = (ubx+lbx)/2;%Begins at the midpoint of the ubx and lbx

        i = index_finder(x(1),x_array);
        
        dx(1) = 0;
        d2x(1) = 0; 
        dV(:,1) = deltaV(:,i); %(4*a-3)*E*phi_spline(:,i)+3*E*phi_spline(:,i).^2-4*a*E*phi_spline(:,i).^3;
        dVx(1) = sum(dV(:,1).*tan_array(:,i)); %tangential component
        
        %tan and normal as functions of rho
        tan_p(:,1) = tan_array(:,i);
        norm_p(:,1) = norm_array(:,i);

        %Solving the ode
        for p = 1:psteps-1
            %p_vector(p+1) = p_vector(p) + dp;
            d2x(p+1) = -(d-1)*dx(p)/p_vector(p+1)+dVx(p);
            dx(p+1) = dx(p)+ d2x(p+1)*dp;
            x(p+1) = x(p) + dx(p)*dp + (d2x(p+1)*dp^2)/2;
            %fprintf('%s\n',x(p+1))

            i = index_finder(x(p+1),x_array);
            
            % Condition if calculated x value is larger than spline path
            % (When the particle has fallen past the hill)
            if i < 0 
                break
            end

            dV(:,p+1) = deltaV(:,i); %(4*a-3)*E*phi_spline(:,i)+3*E*phi_spline(:,i).^2-4*a*E*phi_spline(:,i).^3;
            phi_x_calc(:,p+1) = phi_spline(:,i);
            dVx(p+1) = sum(dV(:,p+1).*tan_array(:,i));
            
            %FOR ACTION

            %tan and normal as a function of rho
            tan_p(:,p+1) = tan_array(:,i);
            norm_p(:,p+1) = norm_array(:,i);

        end

        %Overshoot undershoot condition
        if i < 0
            lbx = (ubx + lbx)/2;
        else
            ubx = (ubx + lbx)/2;
        end
        
        if (n > 100)%&&(i==-1)
            breakout = 1; 
        end
    n = n + 1;
     % PUT PLOTS IN HERE TO SEE ITERATIONS
     
    end
   
    % Use this if you want to input a guess spline

    
    %perpendicular components of the grad of V  (Function of x)
    deltaV_norm = (sum(deltaV(:,1:end-1).*norm_array).*norm_array)./sum(norm_array.^2); %Normal projection vector
    
    %dphi^2/dx^2 
    dphidx = (phi_spline(:,2:end)-phi_spline(:,1:end-1))./(x_array(:,2:end)-x_array(:,1:end-1));
    d2phidx2 = (dphidx(:,2:end)-dphidx(:,1:end-1))./(x_array(2:end-1)-x_array(1:end-2));
    
    % Calculating dxdp for each x value
    for iter = 1:len_x_mat
        i = index_finder(x_array(iter),x);

        if i == -1
            break
        else
            dxdp_calc(iter) = dx(i);
        end

    end
   
    %Normal force calculation
    N_x = ((d2phidx2.*dxdp_calc(1:end-2).^2) - deltaV_norm(:,1:end-1));
    %Perturbation
    max_deltaV = max(sqrt(sum(deltaV.^2)));
    pfactor = phitf/max_deltaV;
    
    %Find Max N_x value for dyn pert fn
    max_old_NF = max_new_NF;
    max_new_NF(1,1) = max(N_x(1,:));
    max_new_NF(2,1) = max(N_x(2,:));
    
%     %***************************PLOTS*****************************************%
    if show < 1
       figure(6) 
        plot(x_array(:,1:end-2),N_x,'.')
        hold on
        title('N vs x')
        xlabel('x')
        ylabel('N')
        legend('N_{i} component','N_{j} component','Location','northwest')
        grid on
%         axis([0 1.6 -2 3])
        hold off

        %Plotting spline with tangent vectors
        figure(7)
        plot(phi_spline(1,:),phi_spline(2,:),'.')
        hold on 
        plot(phi_pert(1,:),phi_pert(2,:),'o')
        plot(phi_pert_prev(1,:),phi_pert_prev(2,:),'k.','MarkerSize',10)
%         quiver(phi_spline(1,qp),phi_spline(2,qp),tan_array(1,qp),tan_array(2,qp))
        quiver(phi_spline(1,qp),phi_spline(2,qp),norm_array(1,qp),norm_array(2,qp))
        xlabel('\phi_x')
        ylabel('\phi_y')
        title('Path')
        grid on
        hold off
        
         figure(8)
         plot(p_vector,x)
         hold on 
         xlabel("\rho")
         ylabel("x")
         title(['x vs \rho'])
         plot(p_vector,trueVacMat)
         h = legend(sprintf('x vs p\nx(1) = %s\n iteration = %d\n',x(1),n),'False Vacuum');
         pos = get(h,'position');
         %h = legend(hAx2, 'sin(x)'); pos = get(h,'position');
         set(h, 'position',[0.6 0.5 pos(3:4)])
         legend boxoff
         hold off
         
        save = input('Do you want to save?');

        show = input(prompt);
        K = input('Perturbation Factor:');
        
        phi_pert_prev = phi_pert;
        
    end
%     if counter > 1000
%         dpert = dpert + 0.0000005
%         counter = 0;
%     end
    
    
    %FUNTION WHICH RETURNS PERTURBED POINTS
    
    
    %saves data
    if (save == 1)
        str_iter = sprintf('%.0f.xlsx',iterations);
        %Saving x_array
        filename1 = strcat('x_array_iter_',str_iter);
        xlswrite(filename1,transpose(x_array))
        %Saving N_x
        filename2 = strcat('N_x_iter_',str_iter);
        xlswrite(filename2,transpose(N_x))
        %Saving Spline
        filename3 = strcat('phi_spline_iter_',str_iter);
        xlswrite(filename3,transpose(phi_spline))
        %Saving x
        filename4 = strcat('x_iter_',str_iter);
        xlswrite(filename4,transpose(x))
        %p_vector
        filename5 = strcat('p_vector_iter_',str_iter);
        xlswrite(filename5,transpose(p_vector))
        %phi_x_calc
        filename6 = strcat('phi_x_calc_iter_',str_iter);
        xlswrite(filename6,transpose(phi_x_calc))
        %tan_array
        filename7 = strcat('tan_array_iter_',str_iter);
        xlswrite(filename7,transpose(tan_array))
        %Normal_array
        filename8 = strcat('norm_array_iter_',str_iter);
        xlswrite(filename8,transpose(norm_array))
        %Guess
        filename9 = strcat('Guess_points_iter_',str_iter);
        xlswrite(filename9,transpose(phi_pert))
        save = 0;
    end
    
    show = show - 1;
 
    
    while deformations < const_iter
        [phi_pert, pert_pnts] = pnts_perturbationV3_thick(pert_num,x,x_array,norm_array,N_x, pfactor,phi_spline,K);
        [x_array,phi_spline] = phi_x(phi_pert, precision, 0, trueVac);
        deformations = deformations + 1;
    end
    deformations = 0;

    
    %Recalculation of tangent and normal vectors
    
    trueVacMat = ones(1,psteps);
    trueVacMat = trueVacMat*x_array(end);
    %Finding tangents of array
    tan_array = et(phi_spline,x_array);
    tan_len = length(tan_array);
    
    %Finding normals of array
    deltaV(1,:) = 2*x_min*phi_spline(1,:).*(alpha*(x_min*phi_spline(1,:)-1).^2+beta*(y_min*phi_spline(2,:)-1).^2-del)+((x_min*phi_spline(1,:)).^2+(y_min*phi_spline(2,:)).^2).*(2*alpha.*(x_min*phi_spline(1,:)-1)) + gamma_i*(0.25*4.*x_min*phi_spline(1,:).^3 - 3*(x_min*phi_spline(1,:).^2)/3);
    deltaV(2,:) = 2*y_min*phi_spline(2,:).*(alpha*(x_min*phi_spline(1,:)-1).^2+beta*(y_min*phi_spline(2,:)-1).^2-del)+((x_min*phi_spline(1,:)).^2+(y_min*phi_spline(2,:)).^2).*(2*beta.*(y_min*phi_spline(2,:)-1)) + gamma_j*(0.25*4.*y_min*phi_spline(2,:).^3 - 3*(y_min*phi_spline(2,:).^2)/3);

    norm_array = en(tan_array,x_array,deltaV);
    
    len_x_mat = length(x_array);
    %Lower and upper bound of inital x
    ubx = x_array(end);
    lbx = 0;



end

%Calculating dphidp for every p value

%Calculating dphidp for every p value

validR = 310; % Uses the valid range of the solution.

% This need to be generalised so the implementation always knows the
% index of the valid solution. 


for iter = 1:psteps
        i = index_finder(x(iter),x_array);

        if i == -1
            break
        else
            dphidx_rho(:,iter) = dphidx(:,i);
        end

end

dphidrho = dphidx_rho.*dx;

%Euclidean Acton
EA = action(p_vector,dphidrho, validR)

%Plots
   figure(6) 
    plot(x_array(:,1:end-2),N_x,'.')
    title('N vs x')
    xlabel('x')
    ylabel('N')
    legend('N_{i} component','N_{j} component','Location','northwest')
   
    %Plotting spline with tangent vectors
    figure(7)
    plot(phi_spline(1,:),phi_spline(2,:),'.')
    hold on 
    xlabel('\phi_x')
    ylabel('\phi_y')
    title('Path')
    grid on
    hold off

    
 %Plots
 figure(2)
 plot(p_vector,x)
 hold on 
 xlabel("\rho")
 ylabel("x")
 title(['x vs \rho'])
 plot(p_vector,trueVacMat)
 h = legend(sprintf('x vs p\nx(1) = %s\n iteration = %d\n',x(1),n),'False Vacuum');
 pos = get(h,'position');
 %h = legend(hAx2, 'sin(x)'); pos = get(h,'position');
 set(h, 'position',[0.6 0.5 pos(3:4)])
 legend boxoff
 hold off
 
 figure(3)     
 plot(p_vector,phi_x_calc(1,:))
 hold on
 plot(p_vector,falseVarray,'LineWidth',3)
 grid on
 xlabel('\rho')
 ylabel('\phi_i')
 title('phi i component vs \rho')
      
 hold off
 
 figure(4)  
 plot(p_vector,phi_x_calc(2,:))
 hold on
 plot(p_vector,falseVarray,'LineWidth',3)
 grid on
 xlabel('\rho')
 ylabel('\phi_j')
 title('phi j component vs \rho')
    
 hold off

%Plotting spline with tangent vectors
figure(5)
plot(phi_spline(1,:),phi_spline(2,:))
hold on 
len = length(phi_spline);
pnt = floor(len/5);
qp = [1, pnt, pnt*2, pnt*3, pnt*4, pnt*5];
 
%Tangent and Normal Plot    
quiver(phi_spline(1,qp),phi_spline(2,qp),tan_array(1,qp),tan_array(2,qp))
quiver(phi_spline(1,qp),phi_spline(2,qp),norm_array(1,qp),norm_array(2,qp))
xlabel('\phi_x')
ylabel('\phi_y')
title('Interpolated Spline with tangetial and normal vectors')
grid on
hold off

figure(6) 
plot(x_array(:,1:end-2),N_x,'.')
title('N vs x')
xlabel('x')
ylabel('N')


    

