%FUNCTION NEEDS TO BE GENERALISED
%SHOULD NORMAL VECTORS ALL POINT IN THE SAME DIRECTION?

%INPUT ARGUMENTS
%et - the tangent vectors in euclidean space for each corresponding x value
%x - the path of phi 
%deltaV - grad of the potential (input is currently unsued)
%**************************************************************************
%OUTPUT
%norm_array - the normal vectors in euclidean space for each corresponding
%x value.

function norm_array = en(et,x,deltaV)

[n,m] = size(et);

normArray_1 = zeros(n,m);
normArray_2 = zeros(n,m);
detdx = zeros(n,m);

detdx(:,1:end-1) = (et(:, 2:m) - et(:, 1:m-1))./(x(2:m)-x(1:m-1));

sign_detdx = sign(detdx);

%Possible normals
normArray_1(1,:) = et(2,:);
normArray_1(2,:) = -et(1,:);

sign_Array_1 = sign(normArray_1); % Used to find which array corresponds to the right sign in numerical deriv

normArray_2(1,:) = -et(2,:);
normArray_2(2,:) = et(1,:);

sign_Array_2 = sign(normArray_2);

C_Array_1 = sign_detdx==sign_Array_1(:,1:end); % Compares the calculated normal signs to the first possibility
C_Array_2 = sign_detdx==sign_Array_2(:,1:end); % Compares the calculated normal signs to the second possibility

%Checks which array has the closest vec
sum_CArray1 = sum(abs(C_Array_1));
isCArray1 = sum_CArray1 == 2*ones(1,m); 

sum_CArray2 = sum(abs(C_Array_2));
isCArray2 = sum_CArray2 == 2*ones(1,m);

%INPUTS THE THE CORRECTED VECTORS
norm_array = isCArray1.*normArray_1(:,1:m) + isCArray2.*normArray_1(:,1:m);%this was changed to all point in the same direction

%CONDITION WHEN BOTH VECTORS CAN BE USED (use Normal array 1)
isNeither = sum(C_Array_1) == ones(1,m);
%**********************%
%For statement which checks what previous direction is
% indicies = find(isNeither);
% for i = indicies
%     if isCArray1(i-1) == 2
%       norm_array(:,i) = normArray_1(:,i);
%     elseif isCArray2(i-1) == 2
%       norm_array(:,i) = normArray_2(:,1:m);
%     else
%         %MUST ACCOUNT FOR IF NO DIRECTION OR STRAIGHT LINE
%     end
% end

norm_array = norm_array + isNeither.*normArray_1(:,1:m);

%**********************%
%STRAIGHT LINE CONDITION (use Normal array 1)
sumStraight = sum(abs(sign_detdx));
isStraight = sumStraight == zeros(1,m);
norm_array = norm_array + isStraight.*normArray_1(:,1:m);

%Cross checking direction with potential NEEDED?
% C_ArrayV1 = sum(abs(sign(deltaV(:,1:m).*norm_array))) ~= -2*ones(1,m);
% norm_array = norm_array.*C_ArrayV1;
%  
% C_ArrayV2 = ~(C_ArrayV1);
% norm_array = norm_array + C_ArrayV2.*normArray_2;
 
magNA = sqrt(sum(norm_array.^2));
 
norm_array = norm_array./magNA;

%*************************************************************************%
% for i = 1:n-2
%     if C_array(i) == 1
%         %Condition for same tan vec
%         detdx(i,1) = et(i,2)/et(i,1); % to account for when the tangent vector isnt found (as no normal is calculated)
%         detdx(i,2) = 1;
%     else
%         for j =1:m
%             detdx(i,j) = (et(i,j+1)-et(i,j))/(x(j+1)-x(j));    
%         end
%     end
% end
% 
% len_detdp = length(detdx);
% 
% magdetdp = zeros(1,n-2);
% 
% for k=1:n-2
%     magdetdp(k) = sqrt(sum(detdx(:,k).^2));
% end
% 
% for k = 1:n-2
%     for d = 1:len_detdp
%         norm_array(k,d) = detdx(k,j)/magdetdp(d);
%     end
% end