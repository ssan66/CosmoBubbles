% Function setup for the tangential equation

% y1 = x
% y2 = dx
% dy1 = dx = y2
% dy2 = d2x = -(alpha/p)*y(2) + dVy

%*******************Description of validX and Flag************************%
%
% validX helps define where the ODE is valid from. It should return the
% x value in which any calculation done after is garbage. 
% validX must be reset to 0 in main for every new calculation of the 
% spline.
%
% Flag turns on when it finds that we are no longer operating within the
% guess splines and changes the function being calculated to avoid any 
% errors.


function  dy_sol = parallelEqn(t,y,d,x_array,dVx)

global validX Flag

dVy = dV_para(y(1),x_array,dVx);

if (isnan(dVy))
    Flag = NaN;
end

if (~isnan(Flag))
    validX = y(1);
    dy(1) = y(2); %dx
    dy(2) = -((d-1)./t)*y(2)+dVy; %d2x
else
    dy(1) = y(2); %dx
    dy(2) = -((d-1)./t)*y(2); %d2x
end

dy_sol = [dy(1);dy(2)];
end



