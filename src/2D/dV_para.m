% Function take in calculates the the RHS of tangential equation

function dV_parallel = dV_para(x,x_array,dVx)

i = index_finder(x,x_array);

%if statement sets the potential to NaN if the x is not in X_array
if (i < 0)||(isnan(x))
    dV_parallel = NaN;
else
    dV_parallel = dVx(i);
end

    



