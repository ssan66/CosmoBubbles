%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Program:      Spline pertubation
%
% Author:       Suresh Sangarapillai, School of Physics, Monash University
%
% Created:      November 21, 2017
%
% Purpose:      The function takes pertubs a "Guessed spline" towards the
%               true spline based on the normal force.
%
% Inputs:       pert_num = the number of points in the guessed spline to be
%               perturbed.
%
%               x = The parameterised curve of phi_i and phi_j which has
%               been previously calculated.
%
%               x_array = The guessed parameterised curve of phi_i and
%               phi_j calculated previously.
%                             
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function [phi_pert, pert_index] = pnts_perturbationV3(pert_num,x,x_array,norm_array,N_x, pfactor,phi_spline,dpert)
    
    %Preallocation
    Thick_Perts = 20;
    xlen = x_array(end);
    xlen_incr = xlen/pert_num;
    x_pert_pnts  = floor(linspace(1,xlen,pert_num)); %Equally space points between the start and end of x
    pert_index = zeros(1,pert_num-3); %gives an array of indicies
    N_xlen = length(N_x);
    count = 0;
    Thick_Count = 0;
   
    
    %End Preallocations
        
    for i = 1:pert_num-2 %Ends a pert_num-2 since Normal force ends two indicies before x
        index = index_finder(xlen_incr*i,x_array);
        count = count + 1;
        if (index >= N_xlen) || (index < 0)
            pert_index(i) = N_xlen;
            break
        else
            pert_index(i) = index;
        end
    end
       
    %CONDITION FOR INITIAL PERTS
    
%     %Thick wall condition
%     if pert_pnts(1) > floor(0.1*N_xlen)
%         %PERTURB SOME INITIAL POINTS
% %         fprintf('initial on\n')
%         
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         Initial_Perts = ones(Thick_Perts);
%         delta_x = x_pert_pnts(1)/Thick_Perts;
%         
%         for i = 1:Thick_Perts
%             index = index_finder(i*delta_x,x_array);
%             Thick_Count = Thick_Count + 1;
%             if index >= pert_pnts(1)
%                 Initial_Perts(i) = pert_pnts(1);
%                 break
%             else
%                 Initial_Perts(i) = index;
%             end
%                 
%         end
       
        %Initial_Perts = floor(linspace(1,floor(pert_pnts(1)),Thick_Perts)); % creates integer array of initial perturbation values
      
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
%         pert_new = zeros(1,count+Thick_Count); 
%         pert_new(1:Thick_Count) = Initial_Perts(1:Thick_Count);
%         pert_new(1+Thick_Count:count+Thick_Count) = pert_pnts(1:count);
%         pert_pnts = pert_new;
%         count = count + Thick_Count;
%     end
     
    
    %END CONDITIONS FOR INITIAL PERTS
    
    %Preallocation
    phi_pert = zeros(2,count+2); %List of perturbed phi points
    %End Preallocation
    
    %Updates middle perturbations points
    pert_pnts_updated = pert_index(1:count);
    
    %Initial and final conditions
    phi_pert(:,1) = phi_spline(:,1);
    phi_pert(:,end) = phi_spline(:,end);
        
    %index_initial = index_finder(x(1),x_array);
    
    %Most likely need to generalise the point near true vacuum
%     avgN_x = sum(N_x(2:6))/5; 
%     phi_pert(:,2) = pfactor.*avgN_x.*dpert.*norm_array(:,6)+phi_spline(:,6); %Completely changes phi_spline start x
%     
    %Does the perturbation require the phi spline to multipled by the
    %perturbation factor and added back on to the spline???
    N_low = (sqrt(sum(N_x.*N_x)) > 0.0001);
    N_x_rescaled = pfactor.*N_x.*N_low;
    phi_pert(:,2:count+1) = N_x_rescaled(:,pert_pnts_updated).*dpert+phi_spline(:,pert_pnts_updated);
    %phi_pert(:,2:count+1) = sqrt(sum((pfactor.*N_x(pert_pnts_updated).*dpert).^2)).*norm_array(:,pert_pnts_updated)+phi_spline(:,pert_pnts_updated);
    
    %Removing any repeated x values
    [~,idx] = unique(phi_pert(1,:),'stable');
    idx_len = length(idx);
    new_phi_pert = zeros(2,idx_len);
    new_phi_pert(:,1:idx_len) = phi_pert(:,idx);
    phi_pert = new_phi_pert;
    
        
end