%This functions searches for the approximate of value in valArray and
%returns its index.

%It returns -1 when the value is not in the array.
function index = index_finder(value,valArray)

len = length(valArray);
ub = len;
lb = 1;
index = floor((ub+lb)/2);
prevIndex = -1;

if value > valArray(end)
     index = -1;
end

while (prevIndex ~= index) 
        
    prevIndex = index;

    if value > valArray(index)
        lb = floor((ub+lb)/2);
    elseif value < valArray(index)
        ub = floor((ub+lb)/2);
    else
        break
    end
    
    index = floor((ub+lb)/2);   
    
end 

    

