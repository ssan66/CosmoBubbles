%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Program:      Spline creator and path parametiser
%
% Author:       Suresh Sangarapillai, Physics Department, Monash University
%
% Created:      September 6, 2017
%
% Purpose:
%
% The functions receives a "guess" of values that define the path phi takes
% to the false vacuum. It then creates a intepolated spline of these values
% which defines the initial guess of the path to the true vacuum.
%**************************************************************************
% phi_guess is a matrix of values that are inputted but the user for the
% spline. 
% precision is the stepsize
% start and end value defines where the curve is defined across
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,phi] = phi_x(phi_guess, precision, startVal, endVal)

% Checks if the input is an array of data values
if ~ismatrix(phi_guess)
    error('Input must be an array')
end

iiflip = startVal:precision:endVal;

%***********************STABILITY NOTE*******************************%
%csaps uses the smoothing algorithm and spline uses exact interpolation
jjflip = csaps(phi_guess(1,:), phi_guess(2,:),1-1e-8,iiflip);%spline(phi_guess(1,:), phi_guess(2,:),iiflip);%csaps(phi_guess(1,:), phi_guess(2,:),1-1e-8,iiflip);

%flip values so the matrix starts at true vacuum
ii = fliplr(iiflip);
jj = fliplr(jjflip);


%To create array output
len = length(ii);
phi = zeros(2,len);
phi(1,:) = ii; 
phi(2,:) = jj;
x = zeros(1,len);
%End 

x(1) = 0;

for iter = 2:len
    x(iter) = sqrt((ii(iter)-ii(iter-1))^2+(jj(iter)-jj(iter-1))^2) + x(iter-1);
end




