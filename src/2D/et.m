%INPUT ARGUMENTS
%phi - A 2d array of phi values that are the initial guess interpolation of
%the part to the false vacuum.
%
%x - the path of phi 
%**************************************************************************
%OUTPUT
%tan_array - the tangent vectors in euclidean space for each corresponding
%x value.


function tan_array = et(phi,x)

    tan_array = (phi(:,2:end) - phi(:,1:end-1))./(x(2:end)-x(1:end-1));
    tan_array = tan_array./sqrt(sum(tan_array.^2));
